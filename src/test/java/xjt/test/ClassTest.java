package xjt.test;


public class ClassTest<X extends Number,Y,Z> {
	private X x;
	public ClassTest(X x){
		this.x = x;
	}
	public X getX(){

		return this.x;
	}
	public static void main(String[]args){
		ClassTest classTest = new ClassTest(2);
		System.out.println(classTest.getX());
	}
}

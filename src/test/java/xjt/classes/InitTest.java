package xjt.classes;

public class InitTest {
	public static void main(String[] args) {
		
		A.say();
		System.out.println(A.value);
	}

}

class B {
	static int value = 100;
	static {
		System.out.println("Class B is initialized."); // 输出
	}
}

class A extends B {
	static {
		System.out.println("Class A is initialized."); // 不会输出
	}
	static void say(){
		value = 250;
		System.out.println("你妹");
	}
}
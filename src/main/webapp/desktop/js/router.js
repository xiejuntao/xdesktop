// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'vm'
], function ($, _, Backbone, Vm) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      //默认路由
       '*actions': 'defaultAction'
    }
  });

  var initialize = function(options){
    var appView = options.appView;
    var router = new AppRouter(options);
    router.on('route:defaultAction', function (actions) {
      require(['views/content/content'], function (DTContentView) {
        var dTContentView = Vm.create(appView, 'DTContentView', DTContentView);
        dTContentView.render();
      });
    });
    Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});

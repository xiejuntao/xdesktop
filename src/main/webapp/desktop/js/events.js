define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
  //复制Backbone.Events对象所有的属性
  var vent = _.extend({}, Backbone.Events);
  return vent;
});
define( "jqd", ["jquery"], function ($) {
	var JQD = (function($, window, document, undefined) {
		  // Expose innards of JQD.
		  return {
		    go: function() {
		      for (var i in JQD.init) {
		        JQD.init[i]();
		      }
		    },
		    init: {
		      frame_breaker: function() {
		        if (window.location !== window.top.location) {
		          window.top.location = window.location;
		        }
		      },
		      //
		      // Initialize the desktop.
		      //
		      desktop: function() {
		        // Alias to document.
		        var d = $(document);
		     
		        //取消指定元素的鼠标按下事件.
		        d.mousedown(function(ev) {
		          var tags = ['a', 'button', 'input', 'select', 'textarea', 'tr'];
		          if (!$(ev.target).closest(tags).length) {
		            JQD.util.clear_active();
		            ev.preventDefault();
		            ev.stopPropagation();
		          }
		        });
		        
		        //取消右击事件
//		        d.on('contextmenu', function() {
//		          return false;
//		        });
				
		        //a链接事件处理
		        d.on('click', 'a', function(ev) {
		          var url = $(this).attr('href');
		          //取得焦点
		          this.blur();
		          //当链接以#开头则阻止自身的事件处理，相当于onclick="javascript:void(0)"?
		          if (url.match(/^#/)) {
		            ev.preventDefault();
		            ev.stopPropagation();
		          }else {
		        	//否则以新的窗口打开链接
		            $(this).attr('target', '_blank');
		          }
		        });

		        //顶部工具的事件处理
		        d.on('mousedown', 'a.menu_trigger', function() {
		          if ($(this).next('ul.menu').is(':hidden')) {
		            JQD.util.clear_active();
		            $(this).addClass('active').next('ul.menu').show();
		          }
		          else {
		            JQD.util.clear_active();
		          }
		        });
		        //顶部工具鼠标滑过事件
		        d.on('mouseenter', 'a.menu_trigger', function() {
		          if ($('ul.menu').is(':visible')) {
		            JQD.util.clear_active();
		            $(this).addClass('active').next('ul.menu').show();
		          }
		        });

		        //桌面图标的鼠标按下事件
		        d.on('mousedown', 'a.icon', function() {
		          //取消其它选中图标的选中状态
		          JQD.util.clear_active();
		          //选中图标
		          $(this).addClass('active');
		        });

		        //桌面图标的双击事件
//		        d.on('dblclick', 'a.icon', function() {
//		          // Get the link's target.
//		          var x = $(this).attr('href');
//		          var y = $(x).find('a').attr('href');
//		          // Show the taskbar button.
//		          if ($(x).is(':hidden')) {
//		            $(x).remove().appendTo('#dock');
//		            $(x).show('fast');
//		          }
//		          // Bring window to front.
//		          JQD.util.window_flat();
//		          $(y).width($(window).width()*0.86);
//		          $(y).height($(window).height()*0.83);
//		          $(y).addClass('window_stack').show();
//		        });
		        //任意拖动桌面图标
//		        d.on('mouseenter', 'a.icon', function() {
//		          $(this).off('mouseenter').draggable({
//		            //revert: true,
//		            //containment: 'parent'
//		          });
//		        });
		        //底部工具标图标的事件处理
		        d.on('click', '#dock a', function() {
		          // Get the link's target.
		          var x = $($(this).attr('href'));
		          // Hide, if visible.
		          if (x.is(':visible')) {
		            x.hide();
		          }
		          else {
		            // Bring window to front.
		            JQD.util.window_flat();
		            x.show().addClass('window_stack');
		          }
		        });

		        //使窗口最前.
		        d.on('mousedown', 'div.window', function() {
		          // Bring window to front.
		          JQD.util.window_flat();
		          $(this).addClass('window_stack');
		        });

		        // 使窗口可拖动.draggable为jquery ui的方法
		        d.on('mouseenter', 'div.window', function() {
		          $(this).off('mouseenter').draggable({
		            // Confine to desktop.
		            // Movable via top bar only.
		            cancel: 'a',
		            containment: 'parent',
		            handle: 'div.window_top'
		          }).resizable({
		            containment: 'parent',
		            minWidth: 400,
		            minHeight: 200
		          });
		        });
		        //双击窗口工具栏，最大或还原窗口大小
		        d.on('dblclick', 'div.window_top', function() {
		          JQD.util.window_resize(this);
		        });
		        //双击窗口工具栏左边的图标快速关闭当前窗口
		        d.on('dblclick', 'div.window_top img', function() {
		          // Traverse to the close button, and hide its taskbar button.
		          /**
		        	*先查找当前img最近的包含有window_top样式的父div,在取此div内的带有window_close
		        	*样式的a链接的href值，此值对应底部工具栏li元素的id。
		        	*最后找到相应的li元素，并"快速"隐藏。
		           */
		          $($(this).closest('div.window_top').find('a.window_close').attr('href')).hide('fast');
		          //查找当前img最近的包含有window_top样式的父div，并隐藏此div
		          $(this).closest('div.window').hide();
		          // Stop propagation to window's top bar.
		          //阻止双击事件的传播?
		          return false;
		        });

		        //最小化窗口，只隐藏当前div，不隐藏底部工具栏的li.
		        d.on('click', 'a.window_min', function() {
		          $(this).closest('div.window').hide();
		        });

		        // 同d.on('dblclick', 'div.window_top')，最大化或还原当前窗口
		        d.on('click', 'a.window_resize', function() {
		          JQD.util.window_resize(this);
		        });
		        //关闭窗口，在此只是隐藏相应的div和li
		        d.on('click', 'a.window_close', function() {
		          $(this).closest('div.window').hide();
		          $($(this).attr('href')).hide('fast');
		        });

		        //底部工具栏最左边的显示或隐藏桌面按钮
		        d.on('mousedown', '#show_desktop', function() {
		          //如果有显示的窗口，则隐藏它们
		          if ($('div.window:visible').length) {
		            $('div.window').hide();
		          }
		          else {
		            //否则查找有没显示的li，若有则获取其相应div的id，通过此id显示对应的div
		            $('#dock li:visible a').each(function() {
		              $($(this).attr('href')).show();
		            });
		          }
		        });
//		        //给窗口的table的奇数tr加上zebra斑马样式。注，下标从0开始。
//		        $('table.data').each(function() {
//		          // Add zebra striping, ala Mac OS X.
//		          $(this).find('tbody tr:odd').addClass('zebra');
//		        });
//		        //给窗口的table的tr加鼠标按下的事件的监听
//		        d.on('mousedown', 'table.data tr', function() {
//		          // Clear active state.
//		          JQD.util.clear_active();
//		          // Highlight row, ala Mac OS X.
//		          $(this).closest('tr').addClass('active');
//		        });
		      },
		      //设置桌面背景
		      wallpaper: function() {
		        // Add wallpaper last, to prevent blocking.
		        if ($('#desktop').length) {
		          $('body').prepend('<img id="wallpaper" class="abs" src="images/misc/wallpaper.jpg" />');
		        }
		      }
		    },
		    //工具
		    util: {
		      //清除table tr的选中状态
		      clear_active: function() {
		        $('a.active, tr.active').removeClass('active');
		        $('ul.menu').hide();
		      },
		      //
		      // Zero out window z-index.
		      // 去除z-index: 10;样式，使窗口不显示在最前面
		      window_flat: function() {
		        $('div.window').removeClass('window_stack');
		      },
		      //
		      // 最大化或还原窗口的位置及大小
		      //
		      window_resize: function(el) {
		        // Nearest parent window.
		        var win = $(el).closest('div.window');
		        //当处于最大化状态
		        if (win.hasClass('window_full')) {
		          // 恢复窗口位置及大小
		          win.removeClass('window_full').css({
		            'top': win.attr('data-t'),
		            'left': win.attr('data-l'),
		            'right': win.attr('data-r'),
		            'bottom': win.attr('data-b'),
		            'width': win.attr('data-w'),
		            'height': win.attr('data-h')
		          });
		        }
		        else {
		          win.attr({
		            //记住当前窗口的大小及位置
		            'data-t': win.css('top'),
		            'data-l': win.css('left'),
		            'data-r': win.css('right'),
		            'data-b': win.css('bottom'),
		            'data-w': win.css('width'),
		            'data-h': win.css('height')
		          }).addClass('window_full').css({
		            //最大化
		            'top': '0',
		            'left': '0',
		            'right': '0',
		            'bottom': '0',
		            'width': '100%',
		            'height': '100%'
		          });
		        }
		        //使窗口处于最前面
		        JQD.util.window_flat();
		        win.addClass('window_stack');
		      }
		    }
		  };})(jQuery, this, this.document);
	return JQD;
} );
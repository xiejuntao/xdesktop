define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/bar_bottom/bottom.html'
], function($, _, Backbone,barBottomViewTemplate){
  var BarBottomView = Backbone.View.extend({
    el: '#bar_bottom',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(barBottomViewTemplate);
    },
    events: {
    	
    }
  });
  return BarBottomView;
});

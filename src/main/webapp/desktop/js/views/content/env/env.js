define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/content/env/env.html'
], function($, _, Backbone,envTemplate){
  var EnvView = Backbone.View.extend({
    el: '#env .window_content',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(envTemplate);
    },
    events: {
    	
    }
  });

  return EnvView;
});
define([
  'jquery',
  'lodash',
  'backbone',
  'views/content/ViewFactory',
  'text!templates/content/content.html'
], function($, _, Backbone,ViewFactory, dTContentTemplate){
  var DTContentView = Backbone.View.extend({
    el: '#desktop',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(dTContentTemplate);
    },
    events: {
    	"dblclick a.icon"  : "showContent",
    	"mouseenter a.icon" : "dragIcon"
    },
    showContent:function(event){
    	var target = $(event.currentTarget);
    	var view = target.attr('href');
    	$(view).width($(window).width()*0.90);
    	$(view).height($(window).height()*0.85);
    	$(view).addClass('window_stack').show();
    	var icon = $(view+"_icon");
    	if ($(icon).is(':hidden')) {
		    $(icon).remove().appendTo('#dock');
		    $(icon).show('fast');
		}
    	ViewFactory.renderView(view.substr(1));
    },
    dragIcon : function(e){
    	$(e.currentTarget).off('mouseenter').draggable({});
  	}
   })
  return DTContentView;
});

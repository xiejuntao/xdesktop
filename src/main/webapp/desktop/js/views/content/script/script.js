define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/content/script/script.html'
], function($, _, Backbone,scriptTemplate){
  var ScriptView = Backbone.View.extend({
    el: '#script .window_content',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(scriptTemplate);
    },
    events: {
    	
    }
  })

  return ScriptView;
});
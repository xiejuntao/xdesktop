define([
  'jquery',
  'underscore',
  'backbone',
  'vm'
], function ($, _, Backbone, Vm) {
	var renderView = function(view,options){
		var mapping = {
			'env':'views/content/env/env',
			'script':'views/content/script/script',
			'terminal':'views/content/terminal/terminal',
			'user':'views/content/user/user'
		};
	  	if(mapping[view]){
		  	require([mapping[view]], function (AbstractView) {
					 var abstractView = Vm.create({},view, AbstractView);
					 abstractView.render();
	      		}
	      	);
	  	}
	}
	return {renderView:renderView};
});
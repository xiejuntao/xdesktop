define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/content/user/user.html'
], function($, _, Backbone,userTemplate){
  var UserView = Backbone.View.extend({
    el: '#user .window_content',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(userTemplate);
    },
    events: {
    	
    }
  })

  return UserView;
});
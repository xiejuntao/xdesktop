define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/bar_top/top.html'
], function($, _, Backbone,barTopViewTemplate){
  var BarTopView = Backbone.View.extend({
    el: '#bar_top',
    initialize: function () {
    },
    render: function () {
      $(this.el).html(barTopViewTemplate);
      this.clock();

    },
    events: {
    },
    clock: function() {
      var clock = $('#clock');
        if (!clock.length) {
          return;
        }
        // Date variables.
        var date_obj = new Date();
        var hour = date_obj.getHours();
        var minute = date_obj.getMinutes();
        var day = date_obj.getDate();
        var year = date_obj.getFullYear();
        var weekday = [
          '周日',
          '周一',
          '周二',
          '周三',
          '周四',
          '周五',
          '周六'
        ];
        // Array for month.
        var month = [
          '一月',
          '二月',
          '三月',
          '四月',
          '五月',
          '六月',
          '七月',
          '八月',
          '九月',
          '十月',
          '十一月',
          '十二月'
        ];
        weekday = weekday[date_obj.getDay()];
        month = month[date_obj.getMonth()];
        var clock_time = weekday + ' ' + hour + ':' + minute + ' ';
        var clock_date = month + ' ' + day + ', ' + year;
        // Shove in the HTML.
        clock.html(clock_time).attr('title', clock_date);
        // Update every 60 seconds.
        setTimeout(this.clock, 60000);
      }
  });
  return BarTopView;
});

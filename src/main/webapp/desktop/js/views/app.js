define([
  'jquery',
  'jui',
  'jqd',
  'lodash',
  'backbone',
  'vm',
  'events',
  'text!templates/layout.html'
], function($,jui,jqd, _, Backbone, Vm, Events, layoutTemplate){
  var AppView = Backbone.View.extend({
    el: '#wrapper',
    initialize: function () {
    },
    render: function () {
      var that = this;
      $(this.el).html(layoutTemplate);
      require(['views/bar_top/top'], function (BarTopView) {
        var barTopView = Vm.create(that, 'bar_top', BarTopView);
        barTopView.render();
      });
      require(['views/bar_bottom/bottom'], function (BarBottomView) {
        var barBottomView = Vm.create(that, 'bar_bottom', BarBottomView);
        barBottomView.render();
      });
      alert("app");
      jqd.go();
    }
  });
  return AppView;
});

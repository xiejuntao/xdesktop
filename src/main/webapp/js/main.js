// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  //给类库起别名
  paths: {
    // Major libraries
    jquery: 'libs/jquery/jquery-1.8.2',
    underscore: 'libs/underscore/underscore-min', // https://github.com/amdjs
    lodash: 'libs/lodash/lodash', // alternative to underscore
    backbone: 'libs/backbone/backbone-min', // https://github.com/amdjs
    sinon: 'libs/sinon/sinon.js',
    // Require.js plugins
    text: 'libs/require/text',//读取模板
    // Just a short cut so we can put our html outside the js dir
    // When you have HTML/CSS designers this aids in keeping them out of the js directory
    templates: '../templates'
  }
});
//程序入口
require([
  'views/app',
  'router',
  'vm'
], function(AppView, Router, Vm){
  //初始views/app视图，统一由VM“创建”
  var appView = Vm.create({}, 'AppView', AppView);
  appView.render();
  Router.initialize({appView: appView});  //router.js初始所有的路由,并默认初始views/dashboard/page
});

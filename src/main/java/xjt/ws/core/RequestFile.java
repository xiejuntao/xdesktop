package xjt.ws.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class RequestFile{
	private MultipartFile multipartFile;
	public RequestFile(MultipartFile multipartFile){
		this.multipartFile = multipartFile;
	}
	public void transferTo(File dest) throws IllegalStateException, IOException{
		multipartFile.transferTo(dest);
	}
	public String getFileType(){
		return multipartFile.getContentType();
	}
	public String getFileName(){
		return multipartFile.getOriginalFilename();
	}
	public long getFileSize(){
		return multipartFile.getSize();
	}
	public InputStream getInputStream() throws IOException{
		return multipartFile.getInputStream();
	}
}

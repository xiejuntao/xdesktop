package xjt.ws.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;


/**
 * 服务请求.
 * @author huzq
 * @author xjt
 */
public class ServiceRequest {
	/**
	 * 头参数：缓存的上次请求时间
	 */
	public static final String HEADER_LAST_MODIFIED_TIME = "last_modified_time";
	/**
	 * POST参数：服务名
	 */
	public static final String POST_PARAM_SERVICE = "SERVICE";
	
	/**
	 * POST参数：头
	 */
	public static final String POST_PARAM_HEADER = "HEADER";
	
	/**
	 * POST参数：接口参数
	 */
	public static final String POST_PARAM_PARAMS = "PARAMS";

	/** Servlet 请求对象. */
	private HttpServletRequest servletRequest;
	
	/**
	 * 完整的服务名
	 */
	private String fullServiceName;
	
	/** 服务名称. */
	private String service;
	
	/** 方法名称. */
	private String method;
	
	/** 头信息. */
	private JSONObject headers;
	
	/** 应用参数. */
	private JSONObject params;
	
	/** 是否有附件上传. */
	private boolean isMultiPart;
	
	/** 附件列表. */
	private Map<String,MultipartFile> fileMap;
	
	
	public ServiceRequest(){	
		
	}
	
	public ServiceRequest(HttpServletRequest servletRequest, String fullServiceName, String service,
			String method, JSONObject headers, JSONObject params,
			boolean isMultiPart,Map<String,MultipartFile> fileMap) {
		super();
		this.servletRequest = servletRequest;
		this.fullServiceName = fullServiceName;
		this.service = service;
		this.method = method;
		this.headers = headers;
		this.params = params;
		this.isMultiPart = isMultiPart;
		this.fileMap = fileMap;
	}
	/**
	 * Gets the servlet 请求对象.
	 *
	 * @return the servlet 请求对象
	 */
	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}
	/**
	 * Gets the 服务名称.
	 *
	 * @return the 服务名称
	 */
	public String getService() {
		return service;
	}
	/**
	 * Gets the 方法名称.
	 *
	 * @return the 方法名称
	 */
	public String getMethod() {
		return method;
	}
	/**
	 * Gets the 头信息.
	 *
	 * @return the 头信息
	 */
	public JSONObject getHeaders() {
		if(headers==null){
			return new JSONObject();
		}
		return headers;
	}
	/**
	 * Gets the 应用参数.
	 *
	 * @return the 应用参数
	 */
	public JSONObject getParams() {
		if(params==null){
			return new JSONObject();
		}
		return params;
	}
	/**
	 * Checks if is 是否有附件上传.
	 *
	 * @return the 是否有附件上传
	 */
	public boolean isMultiPart() {
		return isMultiPart;
	}
	/**
	 * 获取客户端的缓存时间
	 * 
	 * @return
	 */
	public String getLastModifiedTime(){
		return getHeader(HEADER_LAST_MODIFIED_TIME);
	}
	
	/**
	 * 根据关键字获取头信息
	 * @param name
	 * @return
	 */
	public String getHeader(String name){
		if(this.headers!=null && this.headers.containsKey(name)){
			return this.headers.getString(name);
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the 完整的服务名.
	 *
	 * @return the 完整的服务名
	 */
	public String getFullServiceName() {
		return fullServiceName;
	}
	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}
	public void setFullServiceName(String fullServiceName) {
		this.fullServiceName = fullServiceName;
	}
	public void setService(String service) {
		this.service = service;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public void setHeaders(JSONObject headers) {
		this.headers = headers;
	}
	public void setParams(JSONObject params) {
		this.params = params;
	}
	public void setMultiPart(boolean isMultiPart) {
		this.isMultiPart = isMultiPart;
	}
	
	public RequestFile getFile(String param) {
		if(this.fileMap!=null){
			return new RequestFile(fileMap.get(param));
		}
		return null;
	}
	public List<RequestFile> getFiles(){
		List<RequestFile> files = new ArrayList<RequestFile>();
		if(this.fileMap!=null){
			Iterator<MultipartFile> it = fileMap.values().iterator();
			while(it.hasNext()){
				files.add(new RequestFile(it.next()));
			}
		}
		return files;
	}
}

package xjt.ws.core;

import java.io.PrintWriter;
import java.io.StringWriter;


// TODO: Auto-generated Javadoc
/**
 * 服务响应.
 *
 * @author huzq
 */
public class ServiceResponse {
	
	/** 返回参数：详细错误信息. */
	public static final String RESULT_PARAM_ERROR_DETAILS = "ERROR_DETAILS";

	/** 返回参数：错误信息. */
	public static final String RESULTS_PARAM_ERRORS = "ERRORS";

	/** 返回参数：返回值. */
	public static final String RESULTS_PARAM_RESULTS = "RESULTS";

	/** 响应头：文件名. */
	public static final String RESPONSE_HEADER_FILE_NAME = "File-Name";
	
	/** 响应头：文件大小. */
	public static final String RESPONSE_HEADER_FILE_SIZE = "File-Size";
	
	/** 响应头：最新修改时间. */
	public static final String RESPONSE_HEADER_LAST_MODIFIED_TIME = "Last-Modified-Time";
	
	/**
	 *  响应头:文档页数
	 */
	public static final String RESPONSE_HEADER_PAGES = "Pages";
	
	/** 处理是否成功. */
	private boolean success = false;
	
	/** 错误信息. */
	private String errors;
	
	/** 详细错误信息. */
	private String errorDetails;
	
	/** 响应内容是否二进制. */
	private boolean binary = false;

	/** 响应的文件. */
	private ResponseFile responseFile;
	
	/** 响应的对象. */
	private Object results;
	
	/** 是否已经缓存了. */
	private boolean cached;
	
	
	/** 缓存时间. */
	private String lastModifiedTime;

	
	/**
	 * Checks if is 处理是否成功.
	 *
	 * @return the 处理是否成功
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * Sets the 处理是否成功.
	 *
	 * @param success the new 处理是否成功
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * Gets the 错误信息.
	 *
	 * @return the 错误信息
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * Sets the 错误信息.
	 *
	 * @param errors the new 错误信息
	 */
	public void setErrors(String errors) {
		this.errors = errors;
		this.success = false;
	}
	
	/**
	 * 通过异常设置错误信息.
	 *
	 * @param t the new error info
	 */
	public void setErrorInfo(Throwable t){
		this.success = false;
		this.errors = t.getMessage();
		StringWriter out = new StringWriter();
		t.printStackTrace(new PrintWriter(out));
		this.errorDetails = out.toString();
	}
	
	/**
	 * Checks if is 响应内容是否二进制.
	 *
	 * @return the 响应内容是否二进制
	 */
	public boolean isBinary() {
		return binary;
	}

	/**
	 * Sets the 响应内容是否二进制.
	 *
	 * @param binary the new 响应内容是否二进制
	 */
	public void setBinary(boolean binary) {
		this.binary = binary;
	}


	/**
	 * Gets the 响应的对象.
	 *
	 * @return the 响应的对象
	 */
	public Object getResults() {
		return results;
	}

	/**
	 * Sets the 响应的对象.
	 *
	 * @param results the new 响应的对象
	 */
	public void setResults(Object results) {
		this.results = results;
	}

	/**
	 * Gets the 响应的文件.
	 *
	 * @return the 响应的文件
	 */
	public ResponseFile getResponseFile() {
		return responseFile;
	}

	/**
	 * Sets the 响应的文件.
	 *
	 * @param responseFile the new 响应的文件
	 */
	public void setResponseFile(ResponseFile responseFile) {
		this.responseFile = responseFile;
	}
	

	/**
	 * Gets the 详细错误信息.
	 *
	 * @return the 详细错误信息
	 */
	public String getErrorDetails() {
		return errorDetails;
	}

	/**
	 * Sets the 详细错误信息.
	 *
	 * @param errorDetails the new 详细错误信息
	 */
	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	
	/**
	 * Checks if is cached.
	 *
	 * @return true, if is cached
	 */
	public boolean isCached() {
		return cached;
	}

	/**
	 * Sets the cached.
	 *
	 * @param cached the new cached
	 */
	public void setCached(boolean cached) {
		this.cached = cached;
	}

	/**
	 * Gets the 缓存时间.
	 *
	 * @return the 缓存时间
	 */
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	/**
	 * Sets the 缓存时间.
	 *
	 * @param lastModifiedTime the new 缓存时间
	 */
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String toString() {
		return "ServiceResponse [success=" + success + ", errors=" + errors
				+ ", errorDetails=" + errorDetails + ", binary=" + binary
				+ ", responseFile=" + responseFile + ", results=" + results
				+ ", cached=" + cached + ", lastModifiedTime="
				+ lastModifiedTime + "]";
	}

	
}

package xjt.ws.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

// TODO: Auto-generated Javadoc
/**
 * 响应文件.
 *
 * @author huzq
 */
public class ResponseFile {

	/** 文件名. */
	private String fileName;
	
	/** 文件大小(bytes). */
	private long length;
	
	/** 文件输出流. */
	private InputStream inputStream;
	
	/** 最后修改时间. */
	private long lastModifiedTime;
	
	/** 文档页数. */
	private Integer pages;

	/**
	 * 采用文件构造响应文件.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ResponseFile(File file) throws IOException {
		if(file==null){
			throw new IOException("文件对象为Null");
		}
		if(!file.exists()){
			throw new IOException("文件"+file.getName()+"不存在");
		}
		if(file.isDirectory()){
			throw new IOException(file.getName()+"是目录");
		}
		this.fileName = file.getName();
		this.length = file.length();
		this.inputStream = new FileInputStream(file);
		this.lastModifiedTime = file.lastModified();
	}
	
	/**
	 * 构造响应文件对象.
	 *
	 * @param fileName 文件名
	 * @param inputStream 输入流
	 * @param length 文件大小(bytes),未知则为-1
	 * @param lastModifiedTime 文件最近修改时间，未知则为-1
	 */
	public ResponseFile(String fileName,InputStream inputStream,long length,long lastModifiedTime){
		this.fileName = fileName;
		this.inputStream = inputStream;
		this.length = length;
		this.lastModifiedTime = lastModifiedTime;
	}
	
	/**
	 * 构造响应文件对象.
	 *
	 * @param fileName 文件名
	 * @param inputStream 输入流
	 */
	public ResponseFile(String fileName,InputStream inputStream){
		this(fileName,inputStream,-1,-1);	
	}
	
	/**
	 * 构造响应文件对象.
	 *
	 * @param inputStream 输入流
	 */
	public ResponseFile(InputStream inputStream){
		this(null,inputStream);
	}
	
	/**
	 * Gets the 文件名.
	 *
	 * @return the 文件名
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the 文件名.
	 *
	 * @param fileName the new 文件名
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the 文件大小(bytes).
	 *
	 * @return the 文件大小(bytes)
	 */
	public long getLength() {
		return length;
	}

	/**
	 * Sets the 文件大小(bytes).
	 *
	 * @param length the new 文件大小(bytes)
	 */
	public void setLength(long length) {
		this.length = length;
	}

	/**
	 * Gets the 文件输出流.
	 *
	 * @return the 文件输出流
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Sets the 文件输出流.
	 *
	 * @param inputStream the new 文件输出流
	 */
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * Gets the 最后修改时间.
	 *
	 * @return the 最后修改时间
	 */
	public long getLastModifiedTime() {
		return lastModifiedTime;
	}

	/**
	 * Sets the 最后修改时间.
	 *
	 * @param lastModifiedTime the new 最后修改时间
	 */
	public void setLastModifiedTime(long lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	/**
	 * Gets the 文档页数.
	 *
	 * @return the 文档页数
	 */
	public Integer getPages() {
		return pages;
	}

	/**
	 * Sets the 文档页数.
	 *
	 * @param pages the new 文档页数
	 */
	public void setPages(Integer pages) {
		this.pages = pages;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "ResponseFile [fileName=" + fileName + ", length=" + length
				+ ", inputStream=" + inputStream + ", lastModifiedTime="
				+ lastModifiedTime + ", pages=" + pages + "]";
	}
	
	
}

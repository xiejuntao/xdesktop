 package xjt.ws.invoker;

import xjt.ws.core.ServiceRequest;
import xjt.ws.core.ServiceResponse;

/**
 * 
 * 远程服务调用接口，将远程请求转换为本地调用
 * @author huzq
 *
 */
public interface ServiceInvoker {

	/**
	 * 远程服务请求调用
	 * @param reques
	 * @throws Exception 
	 */
	void invoke(ServiceRequest request,ServiceResponse response) throws Exception;
}

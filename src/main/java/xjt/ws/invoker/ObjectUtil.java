package xjt.ws.invoker;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import xjt.ws.core.RequestFile;
import xjt.ws.core.ServiceRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class ObjectUtil {
	@SuppressWarnings("unchecked")
	public static final Object initValue(Class<?> clazz, Type type,
			String paramName, JSONObject jsonParamsValues,
			ServiceRequest request) {
		if (clazz == ServiceRequest.class) {
			return request;
		}
		if (clazz == RequestFile.class) {
			return request.getFile(paramName);
		}
		if (jsonParamsValues == null) {
			return null;
		}
		if (!jsonParamsValues.containsKey(paramName)) {
			return null;
		}
		if(clazz.isArray()){
			return initArrayValue(clazz,type,paramName,jsonParamsValues,request);
		}
		if (clazz == int.class || clazz == Integer.class) {
			return jsonParamsValues.getInteger(paramName);
		}
		if (clazz == boolean.class || clazz == Boolean.class) {
			return jsonParamsValues.getBoolean(paramName);
		}
		if (clazz == byte.class || clazz == Byte.class) {
			return jsonParamsValues.getByte(paramName);
		}
		if (clazz == char.class || clazz == Character.class) {
			String value = jsonParamsValues.getString(paramName);
			if (StringUtils.isEmpty(value)) {
				return null;
			} else {
				return new Character(value.charAt(0));
			}
		}
		if (clazz == short.class || clazz == Short.class) {
			return jsonParamsValues.getShort(paramName);
		}
		if (clazz == long.class || clazz == Long.class) {
			return jsonParamsValues.getLong(paramName);
		}
		if (clazz == float.class || clazz == Float.class) {
			return jsonParamsValues.getFloat(paramName);
		}
		if (clazz == double.class || clazz == Double.class) {
			return jsonParamsValues.getDouble(paramName);
		}
		if (clazz == String.class) {
			return jsonParamsValues.getString(paramName);
		}
		if (List.class.isAssignableFrom(clazz)) {
			Class<?> genericClass = null;
			if (type instanceof ParameterizedType) {
				ParameterizedType pt = (ParameterizedType) type;
				genericClass = (Class<?>) pt.getActualTypeArguments()[0];
			}
			List<JSONObject> jsonObjectList = JSON.toJavaObject(
					jsonParamsValues.getJSONArray(paramName), List.class);
			if (genericClass != null) {
				if(genericClass==RequestFile.class){
					return request.getFiles();
				}
				List<Object> resultList = new ArrayList<Object>();
				for (JSONObject jsonObject : jsonObjectList) {
					resultList.add(JSON.toJavaObject(jsonObject, genericClass));
				}
				return resultList;
			} else {
				return jsonObjectList;
			}
		}
		if (clazz == Date.class) {
			return jsonParamsValues.getDate(paramName);
		}
		if (clazz == BigDecimal.class) {
			return jsonParamsValues.getBigDecimal(paramName);
		}
		if (clazz == BigInteger.class) {
			return jsonParamsValues.getBigInteger(paramName);
		}
		JSONObject jsonObject = jsonParamsValues.getJSONObject(paramName);
		return JSON.toJavaObject(jsonObject, clazz);
	}
	private static final Object initArrayValue(Class<?> clazz, Type type,
			String paramName, JSONObject jsonParamsValues,ServiceRequest request){
		JSONArray jsonArray = jsonParamsValues.containsKey(paramName) ? jsonParamsValues.getJSONArray(paramName) : null;
		String paramTypeName = clazz.getComponentType().getName();
		if (clazz.getComponentType().isPrimitive()) {
			if (paramTypeName.equals("int")) {
				return toArray(jsonArray, int.class);
			} else if (paramTypeName.equals("boolean")) {
				return toArray(jsonArray, boolean.class);
			} else if (paramTypeName.equals("byte")) {
				return  toArray(jsonArray, byte.class);
			} else if (paramTypeName.equals("char")) {
				return toArray(jsonArray, char.class);
			} else if (paramTypeName.equals("short")) {
				return toArray(jsonArray, short.class);
			} else if (paramTypeName.equals("long")) {
				return toArray(jsonArray, long.class);
			} else if (paramTypeName.equals("float")) {
				return toArray(jsonArray, float.class);
			} else if (paramTypeName.equals("double")) {
				return toArray(jsonArray, double.class);
			}
		} else {
			Class<?> arrayClassType = null;
			try {
				arrayClassType = Class.forName(paramTypeName);
			} catch (ClassNotFoundException e) {
				return null;			
			}
			if (arrayClassType.equals(MultipartFile.class)) {
				return request.getFiles();
			} else {
				return toArray(jsonArray, arrayClassType);
			}
		}
		return null;
	}
	private static final Object toArray(JSONArray jsonArray, Class<?> objectClass) {
		if (jsonArray == null) {
			return null;
		} else {
			Object object = JSON.toJavaObject(jsonArray, objectClass);
			return object;
		}
	}
}

package xjt.ws.invoker;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import xjt.ws.cache.LRULinkedHashMap;
import xjt.ws.core.ResponseFile;
import xjt.ws.core.ServiceInvokeServlet;
import xjt.ws.core.ServiceRequest;
import xjt.ws.core.ServiceResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 将服务请求转换成本地Spring Bean调用的实现
 * 
 * @author huzq
 * @author xjt
 */
@Component("serviceInvoker")
public class LocalServiceInvoker implements ApplicationContextAware,
		ServiceInvoker {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(ServiceInvokeServlet.class);
	private ParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
	private LRULinkedHashMap<String,Method> hashMap = new LRULinkedHashMap<String, Method>(100);
	private ApplicationContext context;
	public void invoke(ServiceRequest serviceRequest,
			ServiceResponse serviceResponse) throws Exception {
		Object serviceToCall = null;
		try {
			serviceToCall = context.getBean(
					serviceRequest.getService());
		} catch (BeansException e) {
			logger.error("没有找到名为" + serviceRequest.getService() + "的服务", e);
			serviceResponse.setErrors("没有找到名为" + serviceRequest.getService() + "的服务");
			return;
		}
		Method methodToCall = null;
		String methodName = serviceRequest.getMethod();
		if(hashMap.containsKey(methodName)){
			methodToCall = hashMap.get(methodName);
		}else{
			Method[] methods = serviceToCall.getClass().getMethods();
			for (int i = 0; i < methods.length; i++) {
				if (methods[i].getName().equals(methodName)) {
					methodToCall = methods[i];
					break;
				}
			}
			if (methodToCall == null) {
				serviceResponse.setErrors(serviceToCall.getClass().getName()
						+ "没有找到名为" + methodName + "的方法");
				return;
			}else{
				hashMap.put(methodName, methodToCall);
			}
		}
		logger.debug("serviceToCall:" + serviceToCall.getClass().getName());
		logger.debug("methodToCall:" + methodToCall.getName());

		String[] paramNames = parameterNameDiscoverer
				.getParameterNames(methodToCall);
		JSONObject jsonParamsValues = serviceRequest.getParams();
		Class<?>[] parameterTypes = methodToCall.getParameterTypes();
		Type[] types = methodToCall.getGenericParameterTypes();
		Object[] paramValues = new Object[parameterTypes.length];
		logger.debug("jsonParamValues:" + jsonParamsValues);
		for (int i = 0; i < parameterTypes.length; i++) {
			Class<?> clazz = parameterTypes[i];
			String paramName = paramNames[i];
			Type type = types[i];
			paramValues[i] = ObjectUtil.initValue(clazz, type, paramName, jsonParamsValues, serviceRequest);
		}
		Object result = methodToCall.invoke(serviceToCall, paramValues);
		if (methodToCall.getReturnType().isAssignableFrom(InputStream.class)
				|| methodToCall.getReturnType().isAssignableFrom(File.class)
				|| methodToCall.getReturnType().isAssignableFrom(
						ResponseFile.class)) {
			serviceResponse.setBinary(true);
		} else {
			serviceResponse.setBinary(false);
		}
		if (result != null) {
			ResponseFile responseFile = null;
			if (result instanceof File) {
				responseFile = new ResponseFile((File) result);
			} else if (result instanceof InputStream) {
				responseFile = new ResponseFile((InputStream) result);
			} else if (result instanceof ResponseFile) {
				responseFile = (ResponseFile) result;
			} else {
				serviceResponse.setResults(result);
			}
			serviceResponse.setResponseFile(responseFile);
		}
		serviceResponse.setSuccess(true);
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.context = applicationContext;
	}
}

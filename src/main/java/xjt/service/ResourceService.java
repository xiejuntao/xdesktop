package xjt.service;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import xjt.ws.core.RequestFile;

/**
 * 
 * 资源服务
 * 服务名：AppSupport.Resource
 *
 */
@Service("ResourceService")
public class ResourceService {
	private static final Logger logger = LoggerFactory.getLogger(ResourceService.class);
	public String doSth(String str,RequestFile file1,RequestFile file2) throws IllegalStateException, IOException{
		file2.transferTo(new File("d:/sth.jpg"));
		return "曾经的一个你";
	}
}

package xjt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomBean {
	private int id;
	private String name;
	private Date date;
	private ArrayList<SubBean> subBeans = new ArrayList<SubBean>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public ArrayList<SubBean> getSubBeans() {
		return subBeans;
	}
	public void setSubBeans(ArrayList<SubBean> subBeans) {
		this.subBeans = subBeans;
	}
}

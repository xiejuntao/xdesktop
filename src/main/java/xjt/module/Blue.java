package xjt.module;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "spring_blue")
public class Blue extends IdEntity{
	private String cause;
	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}
}

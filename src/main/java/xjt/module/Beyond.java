package xjt.module;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "spring_beyond")
public class Beyond extends IdEntity{
	private String what;

	public String getWhat() {
		return what;
	}

	public void setWhat(String what) {
		this.what = what;
	}
}

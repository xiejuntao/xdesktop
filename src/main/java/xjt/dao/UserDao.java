package xjt.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import xjt.module.User;

public interface UserDao extends PagingAndSortingRepository<User, Long> {
	User findByLoginName(String loginName);
}

package xjt.hessian.impl;

import xjt.hessian.HelloService;
import xjt.module.User;

public class HelloImpl implements HelloService{
	@Override
	public String sayHello(String username) {
		return "风云：" + username;
	}

	@Override
	public User saveUser(User user) {
		user.setName("自是人生长恨水长东:" + user.getName());
		return user;	
	}
}

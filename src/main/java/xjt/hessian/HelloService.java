package xjt.hessian;

import xjt.module.User;

public interface HelloService {
	public String sayHello(String username);
	public User saveUser(User user);
}
